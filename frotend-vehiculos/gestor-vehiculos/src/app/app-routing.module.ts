import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import {LoginComponent} from './views/login/login.component';
import {GetVehiculosComponent} from './views/get_vehiculos/get_vehiculos.component'
import { NavbarComponent } from './views/navbar/navbar.component';

const routes: Routes = [
  {
    path: '',
    component: LoginComponent
  },
  {
    path: '/listado_vehiculos',
    component: GetVehiculosComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'navbar',
    component: NavbarComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
