import { CanActivate } from '@angular/router';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AppService } from './app.service';

@Injectable()
export class LoginGuard implements CanActivate {

	constructor(private router:Router, private service: AppService) {}

  	canActivate():boolean {
  		if(this.service.get_session()){
  			if(this.service.get_session().token){
  				return true;
  			}
  		}else{
  			this.service.reset_session();
  			this.router.navigateByUrl('/login');
  			return false;
  		}
  	}
}
