import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

// importo los componentes que cree
import { AppRoutingModule } from '../app/app.router';
import { GetVehiculosComponent } from './views/get_vehiculos/get_vehiculos.component';

// importo el servicio
import { AppService } from './app.service';
import { HttpClientModule } from '@angular/common/http';

// formularios de angular
import { FormsModule } from '@angular/forms';
import { LoginComponent } from './views/login/login.component';
import { NavbarComponent } from './views/navbar/navbar.component';
import { LoginGuard } from './app.loginguard';
@NgModule({
  // cuando creo un componente va "declarations"
  declarations: [
    AppComponent,
    GetVehiculosComponent,
    LoginComponent,
    NavbarComponent
  ],
  imports: [
    // modulos cuando importo un componente ya creado con node "imports"
    BrowserModule,
    AppRoutingModule, // importo el modulo
    HttpClientModule,
    FormsModule
  ],
  // cuando creo un servicio va en "providers"
  providers: [
    AppService,
    LoginGuard
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
