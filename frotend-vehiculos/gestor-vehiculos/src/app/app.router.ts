import {Component, NgModule } from '@angular/core';
// importo la libreria router de angular cli
import { Routes, RouterModule } from '@angular/router';

// libreria de componetes
import { CommonModule } from '@angular/common';

// importo el componente views
import {GetVehiculosComponent} from './views/get_vehiculos/get_vehiculos.component';

import { LoginGuard } from './app.loginguard';
import { NavbarComponent } from './views/navbar/navbar.component';
import { LoginComponent } from './views/login/login.component';


// coloco todas las rutas de los componentes
const routes: Routes = [
  {
  path: 'listado_vehiculos',
  component: GetVehiculosComponent,
    canActivate: [LoginGuard]
  },
  {
    path: 'login',
    component: LoginComponent
  },{
    path: 'navbar',
    component: NavbarComponent,
    canActivate: [LoginGuard]
  },
  {
    path: '**',
    component: LoginComponent
  }
]

// creo un router
@NgModule({
  // resive el arreglo de rutas (forRoot)
  imports: [ CommonModule, RouterModule.forRoot(routes) ], // Router=> le paso las rutas
  exports: [ RouterModule ],
  declarations: []
})


// exporto el componente
export class AppRoutingModule {}
