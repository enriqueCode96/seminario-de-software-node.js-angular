import { Injectable } from '@angular/core';
import { Observable, ObservedValueOf } from 'rxjs';
import { HttpClient } from '@angular/common/http';




@Injectable()
  export class AppService {

      // ruta donde ira a buscar el endpoint
      private endpoint: string;

      // constructor de servicio para decirle cual es el servicio
      constructor(private httpCliente: HttpClient){
        // variable de endpint (apunto a mi banckend)
        this.endpoint = 'http://' + window.location.hostname + ':3000/api2';
      }

      get_vehiculos(): Observable <any>{
        // invoco las api del backend (primer servicio)
        return this.httpCliente.get(this.endpoint + '/get_vehiculos',
              {responseType: 'json'});
      }


      insertar_vehiculo( load ): Observable <any>{
        // invoco las api del backend (segundo servicio)
         console.log(load);
          return this.httpCliente.post(this.endpoint + '/insert_vehiculo' , load ,
            {responseType: 'json'});
      }

      // servicio de actualizar
      update_vehiculo(load): Observable<any>{
        //console.log(load)
        return this.httpCliente.put(this.endpoint + '/update_vehiculo',load ,{responseType: 'json'});
      }

      // servicio de eliminar
      delete_vahiculo(load): Observable<any>{
        return this.httpCliente.delete(this.endpoint + '/delete_vehiculo',
           {params: load, responseType: 'json'});
      }

      getCiudades(): Observable<any>{
        return this.httpCliente.get(this.endpoint + '/getciudades' , {responseType: 'json'});
      }
      //app.service.ts
      login(payload):Observable<any>{
      return this.httpCliente.post(this.endpoint + "/login", payload, {responseType: 'json'});
      }

      // local storage en el navegador
      set_session(token){
        localStorage.setItem("vehiculo", JSON.stringify(token));
      }

      set_usuarioLogueado(load){

      }

      reset_session(){
        localStorage.removeItem("vehiculo")
      }


      // trae el item del vehiculo (trae el token de session y me retorna el token)
      get_session(){
        if(localStorage.getItem("vehiculo") && JSON.parse(localStorage.getItem("vehiculo")).token){
            return JSON.parse(localStorage.getItem("vehiculo"));
        }else{
          return false;
        }
      }



  }



