// importo el cor de angular
import { Component, OnInit } from '@angular/core';

// importo el servicio
import { AppService } from '../../app.service';

// libreria rautes (para la navegabilidad en la aplicacion )
import {  } from '@angular/router';

import Swal, {SweetAlertGrow} from 'sweetalert2';



@Component({
  selector: 'get_vehiculos',
  templateUrl: './get_vehiculos.component.html' // especifico cual es la pagina html
})

// exporto la clase con el nombre Get
export class GetVehiculosComponent implements OnInit {

  // arreglo colecion donde guardo todo el listado de vehiculos
  public listadoVehiculos: any [];

  //public listadoCiudades: any [];



  // objeto tipo vehiculo
  public Vehiculo = {
    placa: '',
    color: '',
    marca: '',
    modelo: '',
    //ciudad2: ''
  };

  ngOnInit(): void {
    this.get_vehiculos();
    //this.getCiudades();
  }

  // para inicializar la variable listadoVehiculos
  constructor(public service: AppService){
    this.listadoVehiculos = [];
  }


  // metodo  que me se conecte a un servicio para que me  traiga los vehiculos
  get_vehiculos(): void{
    let response;

    // todo: invocar servicio de datos
    // me subcribo al servicio
    this.service.get_vehiculos().subscribe(
      data => response = data,
      error => {
        console.log('Ocurrio un error al llamar el servicio', error);
      },
      () => {
        this.listadoVehiculos = response;

        // imprimo los resultados en el nevgador
        console.log(this.listadoVehiculos);
      }
    );
  }

  // metodo de borrar
  delete_vehiculo(placa): void{
      let respuesta;
      const load =  { placa : placa };
      this.service.delete_vahiculo(load).subscribe(
        data => respuesta = data,
        error => {
          console.log('Ocurrio un error al llamar el servicio', error);
        },
        () => {
          this.limpiarDatos();
          this.get_vehiculos();
        }
      );
  }


  // metodo de insertar
  insertar_vehiculo(): void {
    let response;
    this.service.insertar_vehiculo(this.Vehiculo).subscribe(
      data => response = data,

      error => {
        console.log('Ocurrio un error al llamar el servicio', error );
      },
      () => {
        // para mostrar un mensaje cunado se acaba de ingresar un vehiculo
        Swal.fire({
          title: '¡Vehiculo Agregado Satisfactoriamente!',
          text: 'Buen Trabajo',
          icon: 'success'
        })
        // limpio los datos del objeto vehiculo
        this.limpiarDatos();
        // para mostrar en la lista el nuevo vehiculo
        this.get_vehiculos();
      }
    );
  }



  /*
  getCiudades(): void{
    this.listadoCiudades = [];

    let respuesta;
    this.service.getCiudades().subscribe(
      data => respuesta = data,
      error => {
        this.listadoCiudades = [];
        console.log('Error al consultar');
      },
      () => {
        this.listadoCiudades = respuesta;
      }
    );

  }*/



  pasarDatosVehiculo( vehiculo ): void{
      this.Vehiculo =  {
        placa: vehiculo.placa,
        color: vehiculo.color,
        marca: vehiculo.marca,
        modelo: vehiculo.modelo,
        //ciudad2: vehiculo.ciudad2
      };
  }

  update_vehiculo(): void{
    let response;
    this.service.update_vehiculo(this.Vehiculo).subscribe(
      data => response = data,
      error => {
        console.log('Ocurrio un error al llamar el servicio', error );
      },
      () => {
        // limpio los datos del objeto vehiculo
        this.limpiarDatos();
        // para mostrar en la lista el nuevo vehiculo
        this.get_vehiculos();
      }
    );
  }


  limpiarDatos(): void{
    this.Vehiculo = {
      placa: '',
      color: '',
      marca: '',
      modelo: '',
      //ciudad2: ''
    };
  }

}
