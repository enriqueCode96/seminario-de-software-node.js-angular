import { Component, OnInit } from '@angular/core';
import { AppService } from 'src/app/app.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(public service: AppService) { }

  ngOnInit(): void {
  }

  logout(){
    // para cerrar la session
    this.service.reset_session();
  }

}
